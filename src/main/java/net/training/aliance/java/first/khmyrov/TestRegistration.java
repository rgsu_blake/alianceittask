package net.training.aliance.java.first.khmyrov;

import java.util.*;

public class TestRegistration {
    private static int a = 0;

    public static void main(String[] args) {
        List<Registration> list = new ArrayList<>();
        long startTime = System.currentTimeMillis();
        String name = "Nik";
        Timer timer1 = new Timer();
        Timer timer2 = new Timer();
        Timer timer3 = new Timer();
        Timer timer4 = new Timer();
        TimerTask timerRegistrate = new TimerTask() {
            @Override
            public void run() {
                synchronized (list) {
                    list.add(new Registration(name + (a++)));
                    if (System.currentTimeMillis() - startTime >= 60000) {
                        timer1.cancel();
                        timer1.purge();
                    }
                }
            }
        };
        timer1.schedule(timerRegistrate, 0, 2100);
        TimerTask timerCount = new TimerTask() {
            @Override
            public void run() {
                synchronized (list) {
                    if (!list.isEmpty()) {
                        for (Registration reg : list) {
                            reg.incCount();
                        }
                    }
                }
            }
        };
        timer2.schedule(timerCount, 0, 1700);
        TimerTask timerRemove = new TimerTask() {
            @Override
            public void run() {
                synchronized (list) {
                    if (!list.isEmpty()) {
                        for (int i = 0; i < list.size(); i++) {
                            if (list.get(i).getCount() >= 4) {
                                list.remove(i);
                            }
                        }
                    } else {
                        timer3.cancel();
                        timer3.purge();
                        timer2.cancel();
                        timer2.purge();
                        timer4.cancel();
                        timer4.purge();
                    }
                }
            }
        };
        timer3.schedule(timerRemove, 0, 1300);
        TimerTask timerPrint = new TimerTask() {
            @Override
            public void run() {
                System.out.println("------------------------------------------------------------");
                synchronized (list) {
                    for (int i = 0; i < list.size(); i++) {
                        System.out.println(list.get(i).toString());
                    }
                }
            }
        };
        timer4.schedule(timerPrint, 0, 1000);
    }
}
