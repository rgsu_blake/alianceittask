package net.training.aliance.java.first.khmyrov;

import java.util.Date;

public class Registration {
    private static int numRegistration = 0;
    private Date timeRegistration = new Date();
    private int count = 0;
    private String name = "Default name";

    public Registration(String name) {
        this.name = name;
        numRegistration++;
    }

    private static int getNumRegistration() {
        return Registration.numRegistration;
    }

    public Date getTimeRegistration() {
        return timeRegistration;
    }

    public int getCount() {
        return count;
    }

    private String getName() {
        return name;
    }

    public void incCount() {
        this.count++;
    }

    @Override
    public String toString() {
        return " №" + getNumRegistration() +
                ", Time=" + getTimeRegistration() +
                ", Count=" + getCount() +
                ", Name='" + getName() + '\'' + " ";
    }
}
